FROM jrei/systemd-debian:10

RUN apt-get update \
    && apt-get install -qy python3 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
